// global data

var playButton = d3.select("#play-button");
var moving = false;
var timer;
var timerCount = 0;
var leap = 10; // amount to jump per tick of clock
// var targetValue = 1034;
var maxCount = 860150 // max count, this num inc by leap each tick of clock
// playspeed smaller is faster
var playSpeed = 5;
// var countLabel = d3.select("#countLabel");
var idLabel = d3.select("#idLabel");
var currentDay = 1;
var dateLabel = d3.select("#dateLabel");
var dateMarker;

// ask the user to use a proper browser
function getBrowser() {
      var ua = navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
      if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
          return {name: 'IE', version: (tem[1] || ''),
          };
      }
      if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/);
          if (tem != null) {
              return {name: 'Opera', version: tem[1]};
          }
      }
      if (window.navigator.userAgent.indexOf("Edge") > -1) {
        tem = ua.match(/\Edge\/(\d+)/);
          if (tem != null) {
              return {name: 'Edge', version: tem[1]};
          }
      }
      M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
      return {
        name: M[0],
        version: +M[1],
      };
}
var browser = getBrowser();
if (browser.name != 'Chrome') {
    if (browser.name != 'Firefox') {
        alert("Unfortunately, this website will not be displayed correctly on " + browser.name + ".\nPlease use Chrome or Firefox");
    }
};

// when the input range changes update the circle
d3.select("#slideMonth").on("input", function(d) {
    timerCount = parseInt(this.value);
    dateLabel.text("Date: " + dayDateMapping[currentDay]);
    update();
});

// map data
colorObj = {'Golden perch': 'green',
    'Murray cod': 'red',
    'Silver perch': 'blue'};

circleObj = [{'fish': 'Golden perch', 'color': 'green', 'y': 10},
    {'fish': 'Murray cod', 'color': 'red', 'y': 24},
    {'fish': 'Silver perch', 'color': 'blue', 'y': 38}];

// setup the leaflet map
var map = L.map('map').setView([-35.5589908, 144.4705083], 10);
mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; ' + mapLink + ' Contributors',
        maxZoom: 16,
        }).addTo(map);

// satalite map toggle
var sataliteToggle = L.easyButton({
    states: [
        {
        stateName: 'add-sat',
        icon: '<ion-icon name="globe"></ion-icon>',
        title: 'Show satalite view',
        onClick: function(control) {
            L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: '&copy; '+mapLink+ ' Contributors',
                maxZoom: 18,
            }).addTo(map);
            control.state('add-map');
        }},
        {
        stateName: 'add-map',
        icon: '<ion-icon name="map"></ion-icon>',
        title: 'Show map view',
        onClick: function(control) {
            L.tileLayer(
                'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; ' + mapLink + ' Contributors',
                    maxZoom: 18,
                }).addTo(map);
            control.state('add-sat');
        }},
    ],
});
// remove comment to add a satelite toggle to the map
// sataliteToggle.addTo(map);

// here is trying to deal with the iframe
// Extract the width and height that was computed by CSS.
var chartDiv = document.getElementById("wrapper");
var wrapperWidth = 600//chartDiv.clientWidth;
var wrapperHeight = 600//chartDiv.clientHeight;
var wrapper = d3.select(chartDiv).append("svg")
    .attr("height", wrapperHeight)
    .attr("width", wrapperWidth)

console.log(wrapperWidth)
console.log(wrapperHeight)

// init the svg layer
map._initPathRoot();
var svg = d3.select("#map").select("svg");
var g = svg.append("g");

var svgkey = d3.select("#map")
    .append("svg")
    .attr('class', 'key');

var elem = svgkey.selectAll('g')
    .data(circleObj);

// create a slot for each cirle+text for the color lable key
var elemEnter = elem.enter()
    .append('g')
    .attr("transform", function(d) {
        return "translate(20,"+d.y+")";
    });

// create a circle for each slot
var circle = elemEnter.append('circle')
    .attr('r', 5)
    .attr('fill', function(d) {
        return d.color;
    });

// create the text for each slot in the color key
elemEnter.append('text')
    .attr("dx", 15)
    .attr("dy", 4)
    .attr('class', 'fishLabels')
    .text(function(d) {
        return d.fish;
    });

var lastValues = {};
var allData = {};
for (var i=0; i<gatePathDictFilt.length; i++) {
    for (var y=0; y<gatePathDictFilt[i].length; y++) {
        var lt = gatePathDictFilt[i][y].LT;
        var lg = gatePathDictFilt[i][y].LG;
        var tempLatLng = new L.LatLng(lt, lg);
        var ctr = gatePathDictFilt[i][y].CTR;
        var fishID = gatePathDictFilt[i][y].id;
        lastValues[fishID] = ctr; // overwrites, should save last val
        var day = gatePathDictFilt[i][y].day;
        if (allData.hasOwnProperty(fishID)) {
            allData[fishID][ctr] = {"fishID": fishID, "day": day,
                "LatLng": tempLatLng, "ctr": ctr};
        } else {
            allData[fishID] = {};
            allData[fishID][ctr] = {"fishID": fishID, "day": day,
                "LatLng": tempLatLng, "ctr": ctr};
        }
    }
}

startData = [];
fishIDArray = Object.keys(allData);
for (var i=0; i< fishIDArray.length; i++) {
    // startData.push(allData[fishIDArray[i]]["2300"]);
    startData.push(allData[fishIDArray[i]]["0"]);
}
// setup the DOM with a circle for each datapoint
var feature = g.selectAll("circle")
    .data(startData)
    .enter()
    .append("circle")
    .attr("r", 2)
    .style("opacity", 0) // need a way to set this ie ctr!=2310 or something
    .on("mouseover", mouseover)
    .on("mouseout", function(d) {
        d3.select(this).attr("r", 2);// .style("fill", "red");
        d3.select(map.getContainer()).select(".tooltip").remove();
        d3.select(this).style("cursor", "grab");
    });

function step() {
    if (moving == true) {
        if (timerCount >= maxCount) {
            moving = false;
            timerCount = 0;
            dateLabel.text("Date: " + dayDateMapping[currentDay]);
            document.getElementById("slideMonth").value = "0";
        } else {
            try {
                for (var i=0; i< fishIDArray.length; i++) {
                    if (allData[fishIDArray[i]].hasOwnProperty(timerCount)) {
                        startData[i] = allData[fishIDArray[i]][timerCount];
                        currentDay = allData[fishIDArray[i]][timerCount].day;
                    }
                }
            } catch (err) {
                // console.log(err);
            }
            // update the data
            feature.data(startData).enter();
            timerCount = timerCount + leap;
            dateLabel.text("Date: " + dayDateMapping[currentDay]);
            document.getElementById("slideMonth").value = timerCount;
            d3.select('#dateLine').remove();
            dateMarker = svgChart.append('line')
                .attr('x1', x(parseDate(dayDateMapping[currentDay])))
                .attr('y1', 0)
                .attr('x2', x(parseDate(dayDateMapping[currentDay])))
                .attr('y2', height )
                .attr('id', 'dateLine')
                .style("stroke-dasharray", ("2, 2"))
                .style('stroke', 'black');
            update();
        }
    }
}

function update() {
    feature.style("fill", function(d) {
        return colorObj[species[d.fishID]];
    })
        .style("opacity", function(d) {
            // set to opac if still of first value
            if (d.ctr == '0') {
                // starting data is invisible
                return 0;
            } else {
                    return 1;
            }
        })
        .attr('r', function(d) {
            if (timerCount < parseInt(lastValues[d.fishID])) {
                return 2;
            } else {
                // console.log('killing' + d.fishID);
                return 0;
            }
        })
        .attr("transform", function(d) {
            return "translate(" +
            map.latLngToLayerPoint(d.LatLng).x + "," +
            map.latLngToLayerPoint(d.LatLng).y + ")";
    }
    );
}

playButton.on("click", function() {
    var button = d3.select(this);
    if (button.text() == "Pause") {
        moving = false;
        clearInterval(timer);
        button.text("Play");
    } else {
        moving = true;
        timer = setInterval(step, playSpeed);
        button.text("Pause");
    }
});

function mouseover(d) {
    idLabel.text(d.fishID + d.LatLng);
    d3.select(this).attr("r", 5);
    var point = map.latLngToContainerPoint(d.LatLng);
    var fishID = d.fishID;
    var speciesLab = species[fishID]
    console.log(fishID);
    var tooltip = d3.select(map.getContainer())
        .append("div")
        .attr("class", "tooltip")
        .style({left: point.x - 20 + "px", top: point.y - 40 -40 + "px"})
        .text(function(d) {
            // return 'test';
          return speciesLab + '; ' + fishID;
        })
        .node();
    d3.select(this).style("cursor", "pointer");
}


// This section is for the chart that is not currently displayed
var margin = {top: 30, right: 50, bottom: 40, left: 80};
var width = 750 - margin.left - margin.right;
var height = 220 - margin.top - margin.bottom;

var parseDate = d3.time.format("%d-%m-%Y").parse;

var x = d3.time.scale().range([0, width]);
var y0 = d3.scale.linear().range([height, 0]);

var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

var yAxisLeft = d3.svg.axis().scale(y0)
    .orient("left").ticks(5);

var valueline = d3.svg.line()
    .x(function(d) {
        return x(d.date);
    })
    .y(function(d) {
        return y0(d.total);
    });

var valueline2 = d3.svg.line()
    .x(function(d) {
        return x(d.date);
    })
    .y(function(d) {
        //return y1(d.temp);
        return y0(d.cew);
    });

var svgChart = d3.select("#chart")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

svgChart.append("text")
    .attr("x", width / 2)
    .attr("y", height / 2)
    .text("Non Commonwealth inflow")
    .style("fill", "steelblue")

svgChart.append("text")
    .attr("x", width / 2 )
    .attr("y", height / 2 + 20)
    .text("Total inflow")
    .style("fill", "tomato")


// Get the data
// d3.csv("static/data/Hydrology_datarange_zone1_weeklysum.csv", function(error, data) {
//d3.csv("static/data/Hydrology_datarange_zone1.csv", function(error, data) {
d3.csv("static/data/Hydrology_datarange_zone1_monthlysum.csv", function(error, data) {
    data.forEach(function(d) {
        d.date = parseDate(d.Date);
        d.total = +d.total_discharge_mL_day;
        d.cew = +d.non_cew_discharge_mL_day;
    });

    // Scale the range of the data
    x.domain(d3.extent(data, function(d) {
        return d.date;
    }));
    y0.domain([0, d3.max(data, function(d) {
        return Math.max(d.total_discharge_mL_day);
    })]);

    svgChart.append("path") // this is the noncew flow
        .style("stroke", "steelblue")
        .attr("opacity", 0.7)
        .attr("d", valueline2(data));

    svgChart.append("path") // this is the total flow
        .style("stroke", "tomato")
        .attr("opacity", 0.7)
        .attr("d", valueline(data));


    svgChart.append("g") // Add the X Axis
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svgChart.append("g")
        .attr("class", "y axis")
        .call(yAxisLeft);

    svgChart.append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0 - margin.left)
        .attr('x', 0 - (height / 2))
        .attr('dy', '1em')
        .attr('class', 'axis-text')
        .style('text-anchor', 'middle')
        .text('Monthly river flow (mL)')

    svgChart.append('text')
        .attr("class", "title")
        .attr('text-anchor', 'middle')
        .attr('x', (width / 2))
        .attr('y', -5)
        .text('River Flow');

    dateMarker = svgChart.append('line')
        .attr('x1', x(parseDate(dayDateMapping[currentDay])))
        .attr('y1', 0)
        .attr('x2', x(parseDate(dayDateMapping[currentDay])))
        .attr('y2', height )
        .attr('id', 'dateLine')
        .style("stroke-dasharray", ("2, 2"))
        .style('stroke', 'black');
});

map.on("viewreset", update);
// Redraw based on the new size whenever the browser window is resized.
window.addEventListener("resize", update);
update();
