# 202101JTHIEM Fish movement animations  

This project is an extension of `201902JTHIEM` with updated data.  

See the `data_prep` folder for notebooks that describe the preparation of the actual data for display as a website. The main files are:  
- `reshape_new_table.ipynb` which reshapes the data from the client  
- `fish_data_preprocessing.ipynb` which creates the JS files required for D3.  

Much of the hard work is described in the old project, however in this attempt I have tried to make the notebooks more cohesive so that it should be easier to repeat this work if the client updates the data again.  

The data files needed to power this app are:  
- `speices.js`  
- `counterSpreadRoundedUpdated.js`  
- `riverData.js`  
- `dayDateMapping.js`  

These are stored in `static/data/`.  
